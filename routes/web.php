<?php

use App\Http\Middleware\CheckGuest;
use App\Http\Middleware\StoreScrollPosition;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('home');
});

Auth::routes();

Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('home');



/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
*/

Route::put('/absence/update', [App\Http\Controllers\AbsenceController::class, 'updateForStudent'])->name('absence.updateForStudent');
Route::get('/students/absence/OKJ', [App\Http\Controllers\AbsenceController::class, 'indexOKJ'])->name('students.absenceOKJ');
Route::get('/students/absence/11', [App\Http\Controllers\AbsenceController::class, 'index11'])->name('students.absence11');
Route::get('/students/absence/12', [App\Http\Controllers\AbsenceController::class, 'index12'])->name('students.absence12');

Route::get('/students', [App\Http\Controllers\StudentController::class, 'index'])->name('students.index');
Route::get('/students/show_deleted', [App\Http\Controllers\StudentController::class, 'show_deleted'])->name('students.show_deleted');
Route::put('/students/restore/{student}', [App\Http\Controllers\StudentController::class, 'restore'])->name('students.restore')->withTrashed();

Route::delete('workdays', [App\Http\Controllers\WorkDayController::class, 'destroy2'])->name('workdays.destroy2');
Route::resource('/workdays', App\Http\Controllers\WorkdayController::class);
Route::resource('/absence', App\Http\Controllers\AbsenceController::class);
Route::resource('/students', App\Http\Controllers\StudentController::class);
