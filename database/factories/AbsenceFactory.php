<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Absence>
 */
class AbsenceFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'status' => fake()->randomElement(['Present', 'Absent', 'Sick']),
            'workday_id' => fake()->numberBetween(1, 93),
            'student_id' => fake()->unique(true)->numberBetween(1, 32),
        ];
    }
}
