<?php

namespace Database\Seeders;

use App\Models\Student;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class StudentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Student::factory(32)->create([
            'course_id' => 1,
        ]);
        Student::factory(32)->create([
            'course_id' => 2,
        ]);
        Student::factory(32)->create([
            'course_id' => 3,
        ]);
    }
}
