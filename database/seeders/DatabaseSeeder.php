<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use App\Models\Absence;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        $this->call([
            CourseSeeder::class,
            StudentSeeder::class,
            WorkDaySeeder::class,
            AbsenceSeeder::class,
        ]);
        \App\Models\User::factory()->create([
            'name' => 'Admin Márton',
            'email' => 'admin@example.com',
            'role_id' => 1,
        ]);
        \App\Models\User::factory()->create([
            'name' => 'Moderátor András',
            'email' => 'mod@example.com',
            'role_id' => 2,
        ]);
    }
}
