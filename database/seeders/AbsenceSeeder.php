<?php

namespace Database\Seeders;

use App\Models\Absence;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class AbsenceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        for ($j = 1; $j <= 31; $j++) {
            for ($i = 1; $i <= 32; $i++) {
                Absence::factory()->create([
                    "workday_id" => $j,
                    "student_id" => $i,
                ]);
            }
        }
        for ($j = 32; $j <= 62; $j++) {
            for ($i = 33; $i <= 64; $i++) {
                Absence::factory()->create([
                    "workday_id" => $j,
                    "student_id" => $i,
                ]);
            }
        }
        for ($j = 63; $j <= 93; $j++) {
            for ($i = 65; $i <= 96; $i++) {
                Absence::factory()->create([
                    "workday_id" => $j,
                    "student_id" => $i,
                ]);
            }
        }
    }
}
