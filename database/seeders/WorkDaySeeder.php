<?php

namespace Database\Seeders;

use App\Models\WorkDay;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class WorkDaySeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        for ($i = 1; $i <= 3; $i++) {

            for ($j = 1; $j <= 31; $j++) {
                if ($j < 10) {
                    WorkDay::factory()->create([
                        'course_id' => $i,
                        'day' => '2024.01.0' . $j
                    ]);
                } else {
                    WorkDay::factory()->create([
                        'course_id' => $i,
                        'day' => '2024.01.' . $j
                    ]);
                }
            }
        }
    }
}
