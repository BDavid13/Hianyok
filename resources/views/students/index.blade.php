@extends('layouts.app')

@section('content')
    <style>
        .table-responsive {
            overflow: hidden !important;
        }

        .dataTables_length {
            display: none;
        }
    </style>
    <div class="row">
        <div class="col-2"></div>
        <div class="col-8">

            <div class="table-responsive">
                <table class="table table-primary" id="myTable">
                    <thead>
                        <tr>
                            <th scope="col">Student name</th>
                            <th scope="col">Student birthdate</th>
                            <th scope="col">Student course</th>
                            <th scope="col" data-orderable="false">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($students as $student)
                            <tr>
                                <td>{{ $student->name }}</td>
                                <td>{{ $student->birthdate }}</td>
                                <td>{{ $student->course->name }}</td>
                                <td class="d-flex">
                                    <form action="{{ route('students.show', $student) }}" method="get">
                                        <button type="submit" class="btn btn-info me-5">Show</button>
                                    </form>
                                    <form action="{{ route('students.edit', $student) }}" method="get">
                                        <button type="submit" class="btn btn-warning ms-5 me-5">Edit</button>
                                    </form>
                                    <form action="{{ route('students.destroy', $student) }}" method="post">
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn btn-danger ms-5">Delete</button>
                                    </form>

                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>

        </div>
        <div class="col-2"></div>
    </div>
    <script>
        let table = new DataTable('#myTable', {
            stateSave: true,
            iDisplayLength: 100,
        });
    </script>
@endsection
