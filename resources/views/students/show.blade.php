@extends('layouts.app')

@section('content')
    <style>
        td:hover {
            cursor: pointer;
        }

        #Present {
            background-color: green;
        }

        html,
        body {
            margin: 0;
            padding: 0;
            font-family: Arial, Helvetica Neue, Helvetica, sans-serif;
            font-size: 14px;
        }

        #external-events {
            z-index: 2;
            width: 300px;
            padding: 0 10px;
            border: 1px solid #ccc;
            background: #eee;
        }

        #external-events .fc-event {
            cursor: move;
            margin: 3px 0;
        }

        #calendar-container {
            position: relative;
            z-index: 1;
            margin-left: 200px;
        }

        #calendar {
            max-width: 1100px;
            margin: 20px auto;
        }
    </style>
    <script>
        document.addEventListener('DOMContentLoaded', function() {
            var Calendar = FullCalendar.Calendar;
            var Draggable = FullCalendar.Draggable;

            var containerEl = document.getElementById('external-events');
            var calendarEl = document.getElementById('calendar');
            var checkbox = document.getElementById('drop-remove');

            // initialize the external events
            // -----------------------------------------------------------------

            new Draggable(containerEl, {
                itemSelector: '#Present',
                eventData: function(eventEl) {
                    return {
                        id: "present",
                        color: 'green',
                        title: eventEl.innerText,
                    };
                }
            });
            new Draggable(containerEl, {
                itemSelector: '#Sick',
                eventData: function(eventEl) {
                    return {
                        id: 'sick',
                        color: 'yellow',
                        title: eventEl.innerText,
                    };
                }
            });
            new Draggable(containerEl, {
                itemSelector: '#Absent',
                eventData: function(eventEl) {
                    return {
                        id: "absent",
                        color: 'red',
                        title: eventEl.innerText,
                    };
                }
            });

            // initialize the calendar
            // -----------------------------------------------------------------
            var eventColor;
            var calendar = new Calendar(calendarEl, {
                headerToolbar: {
                    left: 'prev,next today',
                    center: 'title',
                    right: 'dayGridMonth'
                },
                selectable: true,
                droppable: true, // this allows things to be dropped onto the calendar
                events: [
                    @foreach ($absences as $absence)
                        {

                            title: '{{ $absence->status }}',
                            allDay: true,
                            color: changeColor('{{ $absence->status }}'),
                            start: '{{ $workdays->where('id', '=', $absence->workday_id)->value('day') }}',
                            end: null,
                        },
                    @endforeach
                ],
                drop: function(info) {
                    var events = calendar.getEvents();
                    for (var event of events) {
                        if (event.startStr == info.dateStr) {
                            event.remove();
                            break;
                        }
                    }
                    console.log(info.draggedEl.id);
                    callTheFormSubmission(info);
                }
            });
            function callTheFormSubmission(info){
                document.getElementById('day').setAttribute('value', info.dateStr);
                document.getElementById('status').setAttribute('value', info.draggedEl.id);
                console.log(info.dateStr);
                console.log(info.draggedEl.id);
                document.getElementById('formBtn').click();
            }
            
            function changeColor(status) {
                if (status == "Present") {
                    return 'green';
                } else if (status == "Absent") {
                    return 'red';
                } else {
                    return 'yellow';
                }
            }


            calendar.render();
        });
    </script>
    @if (Session::has('fail'))
        <script>
            alert('This day doesn\'t have a lesson!');
        </script>
    @endif
    <div class="row">

        <div class="col-3"></div>
        <div class="col-6">

            <div class="card border-primary">
                <div class="card-body">
                    <h4 class="card-title">{{ $student->name }}</h4>
                    <p class="card-text">{{ $student->birthdate }}</p>
                    <p class="card-text">{{ $student->course->name }}</p>
                </div>



            </div>
            <div class="col-3"></div>


        </div>
    </div>
    <div class="row">
        <div class="col-1"></div>
        <div class="col-2 mt-5">
            <div id='external-events'>
                <p>
                    <strong>Draggable Events</strong>
                </p>

                <div class='fc-event fc-h-event fc-daygrid-event fc-daygrid-block-event bg-success' id="Present">
                    <div class='fc-event-main'>Present</div>
                </div>
                <div class='fc-event fc-h-event fc-daygrid-event fc-daygrid-block-event bg-warning' id="Sick">
                    <div class='fc-event-main'>Sick</div>
                </div>
                <div class='fc-event fc-h-event fc-daygrid-event fc-daygrid-block-event bg-danger' id="Absent">
                    <div class='fc-event-main'>Absent</div>
                </div>
                <p style="display: none">
                    <input type='checkbox' id='drop-remove' />
                </p>
            </div>
        </div>
        <div class="col-8">
            <div id='calendar-container'>
                <div id='calendar'></div>
                <form action="{{route('absence.updateForStudent', $student)}}" method="POST">
                    @csrf
                    @method('PUT')
                    <input type="hidden" name="day" id="day">
                    <input type="hidden" name="status" id="status">
                    <input type="hidden" name="student_id" value="{{$student->id}}">
                    <input type="hidden" name="course_id" value="{{$student->course_id}}">
                    <button type="submit" class="d-none" id="formBtn"></button>
                </form>
            </div>

            <!-- Cloudflare Pages Analytics -->
            <script defer src='https://static.cloudflareinsights.com/beacon.min.js'
                data-cf-beacon='{"token": "dc4641f860664c6e824b093274f50291"}'></script><!-- Cloudflare Pages Analytics -->

        </div>
        @foreach ($absences as $absence)
            <script>
                var dateStatus = document.querySelector(
                    '[data-date="{{ $workdays->where('id', '=', $absence->workday_id)->value('day') }}"]');
                var event = {
                    id: '{{ $absence->status }}',
                    title: '{{ $absence->status }}',
                    start: {
                        date: '{{ $workdays->where('id', '=', $absence->workday_id)->value('day') }}',
                    },
                    end: {
                        date: '{{ $workdays->where('id', '=', $absence->workday_id)->value('day') }}',
                    }
                };
                var Calendar = FullCalendar.Calendar;
                var calendarEl = $('#calendar');
                var calendar = new Calendar(calendarEl, {
                    timeZone: 'UTC',
                    events: [{
                        id: '{{ $absence->status }}',
                        title: '{{ $absence->status }}',
                        allDay: true,
                    }]
                })
            </script>
        @endforeach
    </div>
@endsection
