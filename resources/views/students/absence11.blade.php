@extends('layouts.app')

@section('content')
    <style>
        .table-responsive {
            overflow: hidden !important;
        }

        .dataTables_length {
            display: none;
        }

        .modal {
            display: none;
            /* Hidden by default */
            position: fixed;
            /* Stay in place */
            z-index: 1;
            /* Sit on top */
            left: 0;
            top: 0;
            width: 100%;
            /* Full width */
            height: 100%;
            /* Full height */
            overflow: auto;
            /* Enable scroll if needed */
            background-color: rgb(0, 0, 0);
            /* Fallback color */
            background-color: rgba(0, 0, 0, 0.4);
            /* Black w/ opacity */
        }

        /* Modal Content/Box */
        .modal-content {
            background-color: #fefefe;
            margin: 15% auto;
            /* 15% from the top and centered */
            padding: 20px;
            border: 1px solid #888;
            width: 80%;
            /* Could be more or less, depending on screen size */
        }

        /* The Close Button */
        .close {
            color: #aaa;
            float: right;
            font-size: 28px;
            font-weight: bold;
        }

        .close:hover,
        .close:focus {
            color: black;
            text-decoration: none;
            cursor: pointer;
        }

        .absence_table td,
        th {
            border: 1px solid black;
        }

        td {
            background-color: transparent;
            color: black;
            transition: all 0.2s ease-in-out;
        }

        td:hover {
            background-color: black !important;
            color: white !important;
            cursor: pointer;
        }

        .modalbtn {
            background-color: transparent;
            color: black;
            transition: all 0.2s ease-in-out;
        }

        .modalbtn:hover {
            background-color: rgba(0, 0, 0, 0.1) !important;
        }
    </style>

    <div class="row">
        <div class="col-4"></div>
        <div class="col-4">
            @foreach ($workdays as $workday)
            <h1 class="text-center mb-5">{{$workday->day}}</h1>
                <form action="{{ route('workdays.update', $workday) }}" method="post" id="editForm">
                    @csrf
                    @method('PUT')
                    <div class="row mb-3">
                        <div class="col-4">
                            <input type="hidden" name="workday_id" value="{{$workday->id}}">
                            <input type="hidden" name="course_id" value="{{$workday->course_id}}">
                            <input type="text" name="lesson_name" id="les_names_inputos"
                                style="font-size: 20px"class="d-none form-control"
                                value="{{ $workday->lesson_name }}">
                            <h4 id="les_names_headeres" style="transform: translateY(20%)">{{ $workday->lesson_name }}</h4>
                        </div>
                        <div class="col-5">
                            <button class="btn btn-info ms-5" onclick="editLesson()" id="les_names_buttones">Edit lesson name</button>
                        </div>
                        <div class="col-3"></div>
                    </div>
                </form>
            @endforeach
            <div class="table-responsive">
                <table class="table table-primary absence_table" id="myTable" stateSave="true">
                    <thead>
                        <tr>
                            <th scope="col" class="text-center">
                                <h4>Student Name</h4>
                            </th>
                                <th data-orderable="false" class="text-center">
                                    <h4>Status</h4>
                                </th>
                        </tr>
                    </thead>
                    <tbody class="text-center">


                        @foreach ($students as $student)
                            <tr>
                                <th>
                                    {{ $student->name }}
                                </th>
                                @foreach ($workdays as $workday)
                                    <td class="text-center" onclick="openModal(this)" for="{{ $workday->id }}"
                                        value="{{ $student->id }}"
                                        @if ($student->absence->where('workday_id', '=', $workday->id)->value('status') == 'Absent') style="background-color: red" 
                                    @elseif ($student->absence->where('workday_id', '=', $workday->id)->value('status') == 'Sick') style="background-color: yellow" 
                                    @elseif ($student->absence->where('workday_id', '=', $workday->id)->value('status') == 'Present') style="background-color: green" @endif>
                                        {{ $student->absence->where('workday_id', '=', $workday->id)->value('status') }}
                                    </td>
                                @endforeach

                            </tr>
                            <?php $day = ''; ?>
                            <!-- The Modal -->
                            <div id="myModal" class="modal">

                                <!-- Modal content -->
                                <div class="modal-content" style="width: 50%">
                                    <span class="close">&times;</span>

                                    <div class="row">
                                        <div class="col-2"></div>
                                        <div class="col-8">
                                            <div class="row d-flex">

                                                <div class="col-3 text-center me-3 modalbtn"
                                                    style="border: 1px solid black; height: 50px; background-color: green; font-weight: bold; cursor: pointer">
                                                    <form action="{{ route('absence.update', $student) }}" method="post"
                                                        name="updata" id="updata">
                                                        @csrf
                                                        @method('PUT')
                                                        <input type="hidden" name="student" value="Present" />
                                                        <input type="hidden" name="userid" id="userid1">
                                                        <button type="submit" name="formSub" id="formSub1"
                                                            style="border: none; background-color:transparent; width: 100%"
                                                            onclick="changeState1()">
                                                            <p style="transform: translateY(50%)">Present</p>
                                                        </button>
                                                    </form>
                                                </div>
                                                <div class="col-3 text-center me-3 modalbtn"
                                                    style="border: 1px solid black; height: 50px; background-color: yellow; font-weight: bold; cursor: pointer">
                                                    <form action="{{ route('absence.update', $student) }}" method="post"
                                                        name="updata" id="updata">
                                                        @csrf
                                                        @method('PUT')
                                                        <input type="hidden" name="student" value="Sick" />
                                                        <input type="hidden" name="userid" id="userid2">
                                                        <button type="submit" name="formSub" id="formSub2"
                                                            style="border: none; background-color:transparent; width: 100%"
                                                            onclick="changeState2()">
                                                            <p style="transform: translateY(50%)">Sick</p>
                                                        </button>
                                                    </form>
                                                </div>
                                                <div class="col-3 text-center modalbtn"
                                                    style="border: 1px solid black; height: 50px; background-color: red; font-weight: bold; cursor: pointer">
                                                    <form action="{{ route('absence.update', $student) }}" method="post"
                                                        name="updata" id="updata">
                                                        @csrf
                                                        @method('PUT')
                                                        <input type="hidden" name="student" value="Absent" />
                                                        <input type="hidden" name="userid" id="userid3">
                                                        <button type="submit" name="formSub" id="formSub3"
                                                            style="border: none; background-color:transparent; width: 100%"
                                                            onclick="changeState3()">
                                                            <p style="transform: translateY(50%)">Absent</p>
                                                        </button>
                                                    </form>
                                                </div>
                                                <div class="col-2"></div>
                                            </div>
                                        </div>
                                        <div class="col-2"></div>
                                    </div>

                                </div>

                            </div>
                        @endforeach

                    </tbody>
                </table>
            </div>

        </div>
        <div class="col-4"></div>
    </div>


    <script>
        let table = new DataTable('#myTable', {
            stateSave: true,
            iDisplayLength: 100,
        });

        var modal = document.getElementById("myModal");

        // Get the <span> element that closes the modal
        var span = document.getElementsByClassName("close")[0];
        let cell = "";


        // When the user clicks on the button, open the modal

        function openModal(item) {
            modal.style.display = "block";
            item.setAttribute('id', 'selected');
        }

        // When the user clicks on <span> (x), close the modal
        span.onclick = function() {
            modal.style.display = "none";
        }

        // When the user clicks anywhere outside of the modal, close it
        window.onclick = function(event) {
            if (event.target == modal) {
                modal.style.display = "none";
            }
        }

        function changeState1() {
            document.getElementById("selected").innerHTML = "Present";
            document.getElementById("selected").style.backgroundColor = 'green';

            var param = document.getElementById("selected").getAttribute('for');
            var userID = document.getElementById("selected").getAttribute('value');
            document.getElementById("userid1").setAttribute('value', userID);
            document.getElementById("formSub1").setAttribute('value', param);
            document.getElementById("selected").removeAttribute('id', "selected");
            modal.style.display = "none";
        }

        function changeState2() {
            document.getElementById("selected").innerHTML = "Sick";
            document.getElementById("selected").style.backgroundColor = 'yellow';

            var param = document.getElementById("selected").getAttribute('for');
            var userID = document.getElementById("selected").getAttribute('value');
            document.getElementById("userid2").setAttribute('value', userID);
            document.getElementById("formSub2").setAttribute('value', param);
            document.getElementById("selected").removeAttribute('id', "selected");
            modal.style.display = "none";
        }

        function changeState3() {
            document.getElementById("selected").innerHTML = "Absent";
            document.getElementById("selected").style.backgroundColor = 'red';

            var param = document.getElementById("selected").getAttribute('for');
            var userID = document.getElementById("selected").getAttribute('value');
            document.getElementById("userid3").setAttribute('value', userID);
            document.getElementById("formSub3").setAttribute('value', param);
            document.getElementById("selected").removeAttribute('id', "selected");
            modal.style.display = "none";
        }

        const button = document.getElementById("les_names_buttones");

        button.addEventListener("click", function(event) {
            event.preventDefault();
        });

        const form = document.querySelector('#editForm');

        function editLesson() {
            document.getElementById('les_names_headeres').classList.add('d-none');
            document.getElementById('les_names_inputos').classList.remove('d-none');

            if (button.innerHTML == "Save") {
                form.submit();
            }
            else{
                button.innerHTML = "Save";
            }
        }
    </script>
@endsection
