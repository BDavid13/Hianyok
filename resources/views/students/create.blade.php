@extends('layouts.app')

@section('content')

    <div class="row">

        <div class="col-4"></div>
        <div class="col-4">

            <div class="card border-primary">
                @if ($errors->any())
                    <div class="mb-3 mt-3">
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                            <strong>Holy guacamole!</strong>

                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                @endif
                @if (Session::has('success'))
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                         
                            <strong>Holy guacamole!</strong> 
                            <p>{{Session::get('success')}}</p>
                        </div>
                    @endif


                <div class="card-body">
                    <h4 class="card-title">Add student</h4>
                    <form action="{{ route('students.store') }}" method="post">
                        @csrf
                        @method('POST')
                        <div class="form-group p-5">
                            <div class="row">
                                <label for="name">Student's name</label>
                                <input type="text" name="name" id="name" class="form-control">
                            </div>
                            <div class="row mt-4">
                                <label for="birthdate">Student's date of birth</label>
                                <input type="date" name="birthdate" id="birthdate" class="form-control">
                            </div>
                            <div class="row mt-4">
                                <label for="course_id">Course</label>
                                <div>
                                    <input type="radio" id="course_id1" name="course_id" value="1" />
                                    <label for="course_id">OKJ</label>
                                  </div>
                                
                                  <div>
                                    <input type="radio" id="course_id2" name="course_id" value="2" />
                                    <label for="course_id">11.</label>
                                  </div>
                                
                                  <div>
                                    <input type="radio" id="course_id3" name="course_id" value="3" />
                                    <label for="course_id">12.</label>
                                  </div>
                            </div>
                            <div class="row d-flex justify-content-center mt-5">
                                <button type="submit" class="btn btn-info w-50 text-white">Add student</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>


        </div>
        <div class="col-4"></div>

    </div>

@endsection
