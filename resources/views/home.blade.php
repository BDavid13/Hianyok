@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('Dashboard') }}</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        <h1>Info!</h1>
                        <p>Inkább magyarul írom ide a dolgokat, de az egész projekt angol. <br><br>A <b>"Students"</b>
                            menüpont a
                            tanulók megtekintésére szolgál, itt lehet szerkeszteni és törölni a tanulókat, plusz
                            újdonságként <b>szerkeszthető az egyén hiányzása drag 'n' drop eventekkel</b><br><br>A
                            <b>"Calendar"</b> menü a kurzusok azon napi helyzetének megtekintésére szolgál, avagy aznapi
                            adatok törlésére. A menü megnyitásához kattints egy napra, majd a menüből eldöntheted, hogy
                            törölsz vagy megtekinted... Oh, hogy nincs egy kurzus az adott napon? Nem baj! Kattints a
                            kurzusra amelyet szeretnéd hogy ott legyen, a többi meg magától jön. A megjeleített táblázatban
                            is lehet módosítani a tanulók hiányzását kattintással<br><br>Igen, a frontend
                            nem a legjobb, de a backend legalább megbízható :D
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
