@extends('layouts.app')

@section('content')

    <div class="row">

        <div class="col-4"></div>
        <div class="col-4">

            <div class="card border-primary">
                @if ($errors->any())
                    <div class="mb-3 mt-3">
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                            <strong>Holy guacamole!</strong>

                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                @endif
                @if (Session::has('success'))
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                         
                            <strong>Holy guacamole!</strong> 
                            <p>{{Session::get('success')}}</p>
                        </div>
                    @endif


                <div class="card-body">
                    <h4 class="card-title">Add lesson for {{$day}} <br> <br>for the course: {{$course}}</h4>
                    <form action="{{ route('workdays.store') }}" method="post">
                        @csrf
                        @method('POST')
                        <div class="form-group p-5">
                            <div class="row">
                                <label for="name">Lesson Name</label>
                                <input type="text" name="lesson" id="lesson" class="form-control">
                                <input type="hidden" name="day" value="{{$day}}">
                                <input type="hidden" name="course_id" value="{{$course_id}}">
                            </div>
                            <div class="row d-flex justify-content-center mt-5">
                                <button type="submit" class="btn btn-info w-50 text-white">Add Lesson</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>


        </div>
        <div class="col-4"></div>

    </div>

@endsection
