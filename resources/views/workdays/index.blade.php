@extends('layouts.app')

@section('content')
    <style>
        td:hover {
            cursor: pointer;
        }

        .fc-daygrid-event-harness {
            z-index: -10000 !important;
        }

        /* The Modal (background) */
        .modal {
            display: none;
            /* Hidden by default */
            position: fixed;
            /* Stay in place */
            z-index: 1;
            /* Sit on top */
            left: 0;
            top: 0;
            width: 100%;
            /* Full width */
            height: 100%;
            /* Full height */
            overflow: auto;
            /* Enable scroll if needed */
            background-color: rgb(0, 0, 0);
            /* Fallback color */
            background-color: rgba(0, 0, 0, 0.4);
            /* Black w/ opacity */
        }

        /* Modal Content/Box */
        .modal-content {
            background-color: #fefefe;
            margin: 15% auto;
            /* 15% from the top and centered */
            padding: 20px;
            border: 1px solid #888;
            width: 80%;
            /* Could be more or less, depending on screen size */
        }

        /* The Close Button */
        .close {
            color: #aaa;
            float: right;
            font-size: 28px;
            font-weight: bold;
        }

        .close:hover,
        .close:focus {
            color: black;
            text-decoration: none;
            cursor: pointer;
        }
    </style>
    @if(Session::has('fail'))
        <script>
            alert('ERROR! There\'s already a lesson for this day and course!')
        </script>
    @endif
    <div class="row">
        <div class="col-3"></div>
        <div class="col-6">

            <div id='calendar'></div>
            <!-- Cloudflare Pages Analytics -->
            <script defer src='https://static.cloudflareinsights.com/beacon.min.js'
                data-cf-beacon='{"token": "dc4641f860664c6e824b093274f50291"}'></script><!-- Cloudflare Pages Analytics -->

            <!-- Trigger/Open The Modal -->
            <button id="myBtn" style="display: none">Open Modal</button>

            <!-- The Modal -->
            <div id="myModal" class="modal">

                <!-- Modal content -->
                <div class="modal-content">
                    <span class="close">&times;</span>
                    <h4 id="titleForModal">View</h4>
                    <div class="row">
                        <div class="col-3"></div>
                        <div class="row">
                            <div class="col-3" id="tweedleDee1">
                                <form action="{{ route('students.absenceOKJ') }}" method="GET">
                                    <input type="hidden" name="day" id="day1">
                                    <button type="submit" class="btn btn-info" onclick="getDay1()">OKJ</button>
                                </form>
                            </div>
                            <div class="col-3" id="tweedleDee2">
                                <form action="{{ route('students.absence11') }}" method="GET">
                                    <input type="hidden" name="day" id="day2">
                                    <button type="submit" class="btn btn-info" onclick="getDay2()">11</button>
                                </form>
                            </div>
                            <div class="col-3" id="tweedleDee3">
                                <form action="{{ route('students.absence12') }}" method="GET">
                                    <input type="hidden" name="day" id="day3">
                                    <button type="submit" class="btn btn-info" onclick="getDay3()">12</button>
                                </form>
                            </div>
                        </div>

                        <h4 class="mt-5">Remove course</h4>
                        <div class="row">
                            <div class="col-3" id="tweedleDum1">
                                <form action="{{ route('workdays.destroy2') }}" method="POST">
                                    @csrf
                                    @method('DELETE')
                                    <input type="hidden" name="day" id="Tday1">
                                    <input type="hidden" name="course_id" value='1' id="course_id1">
                                    <button type="submit" class="btn btn-danger" onclick="TgetDay1()">OKJ</button>
                                </form>
                            </div>
                            <div class="col-3" id="tweedleDum2">
                                <form action="{{ route('workdays.destroy2') }}" method="POST">
                                    @csrf
                                    @method('DELETE')
                                    <input type="hidden" name="day" id="Tday2">
                                    <input type="hidden" name="course_id" value='2' id="course_id2">
                                    <button type="submit" class="btn btn-danger" onclick="TgetDay2()">11</button>
                                </form>
                            </div>
                            <div class="col-3" id="tweedleDum3">
                                <form action="{{ route('workdays.destroy2') }}" method="POST">
                                    @csrf
                                    @method('DELETE')
                                    <input type="hidden" name="day" id="Tday3">
                                    <input type="hidden" name="course_id" value='3' id="course_id3">
                                    <button type="submit" class="btn btn-danger" onclick="TgetDay3()">12</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

            <div class="col-3"></div>
            <script>
                // Get the modal
                var modal = document.getElementById("myModal");

                // Get the button that opens the modal
                var btn = document.getElementById("myBtn");

                // Get the <span> element that closes the modal
                var span = document.getElementsByClassName("close")[0];

                // When the user clicks on the button, open the modal
                btn.onclick = function() {
                    modal.style.display = "block";
                }

                // When the user clicks on <span> (x), close the modal
                span.onclick = function() {
                    modal.style.display = "none";
                }

                // When the user clicks anywhere outside of the modal, close it
                window.onclick = function(event) {
                    if (event.target == modal) {
                        modal.style.display = "none";
                    }
                }
                var dates = [];
                @foreach ($workdays as $workday)
                    @if ($workday->course_id == 1)
                        var obj = {
                            day: '{{ $workday->day }}',
                            course_id: 1,
                        };
                    @elseif ($workday->course_id == 2)
                        var obj = {
                            day: '{{ $workday->day }}',
                            course_id: 2,
                        };
                    @elseif ($workday->course_id == 3)
                        var obj = {
                            day: '{{ $workday->day }}',
                            course_id: 3,
                        };
                    @endif
                    dates.push(obj);
                @endforeach
                var select = "";

                document.addEventListener('DOMContentLoaded', function() {
                    var calendarEl = document.getElementById('calendar');
                    var calendar = new FullCalendar.Calendar(calendarEl, {
                        selectable: true,
                        headerToolbar: {
                            left: 'prev,next today',
                            center: 'title',
                            right: 'dayGridMonth'
                        },
                        events: [
                            @foreach ($workdays as $workday)
                                {

                                    title: '{{ $workday->course->name }}',
                                    allDay: true,
                                    start: '{{ $workday->day }}',
                                    end: null,
                                },
                            @endforeach
                        ],
                        dateClick: function(info) {
                            select = String(info.dateStr);
                            console.log(select);
                            btn.click();

                        },
                    });

                    calendar.render();
                });


                function getDay1() {
                    document.getElementById('day1').setAttribute('value', select);
                }

                function getDay2() {
                    document.getElementById('day2').setAttribute('value', select);
                }

                function getDay3() {
                    document.getElementById('day3').setAttribute('value', select);
                }


                function TgetDay1() {
                    document.getElementById('Tday1').setAttribute('value', select);
                }

                function TgetDay2() {
                    document.getElementById('Tday2').setAttribute('value', select);
                }

                function TgetDay3() {
                    document.getElementById('Tday3').setAttribute('value', select);
                }
            </script>
        </div>
    @endsection
