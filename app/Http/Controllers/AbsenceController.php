<?php

namespace App\Http\Controllers;

use App\Http\Requests\ShowIndexRequest;
use App\Http\Requests\UpdateAbsenceRequestElectricBogaloo;
use App\Models\Absence;
use App\Http\Requests\StoreAbsenceRequest;
use App\Http\Requests\UpdateAbsenceRequest;
use App\Models\Student;
use App\Models\Course;
use App\Models\WorkDay; 
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class AbsenceController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function indexOKJ(ShowIndexRequest $request)
    {
        $students = Student::all()->where('course_id', '=', 1);
        $workdays = WorkDay::all()->where('day', '=', $request->day)->where('course_id', '=', 1);
        if(count($workdays = WorkDay::all()->where('day', '=', $request->day)->where('course_id', '=', 1)) == 0){
            $day = $request->day;
            $course_id = 1;
            $course = Course::all()->where('id', '=', 1)->value('name');
            return view('workdays.create', compact('day', 'course_id', 'course'));
        }
        $absences = Absence::all();
        return view('students.absenceOKJ', compact('students', 'workdays', 'absences'));
    }
    public function index11(ShowIndexRequest $request)
    {
        $students = Student::all()->where('course_id', '=', 2);
        $workdays = WorkDay::all()->where('day', '=', $request->day)->where('course_id', '=', 2);
        if(count($workdays = WorkDay::all()->where('day', '=', $request->day)->where('course_id', '=', 2)) == 0){
            $day = $request->day;
            $course_id = 2;
            $course = Course::all()->where('id', '=', 2)->value('name');
            return view('workdays.create', compact('day', 'course_id', 'course'));
        }
        $absences = Absence::all();
        return view('students.absence11', compact('students', 'workdays', 'absences'));
    }
    public function index12(ShowIndexRequest $request)
    {
        $students = Student::all()->where('course_id', '=', 3);
        $workdays = WorkDay::all()->where('day', '=', $request->day)->where('course_id', '=', 3);
        if(count($workdays = WorkDay::all()->where('day', '=', $request->day)->where('course_id', '=', 3)) == 0){
            $day = $request->day;
            $course_id = 3;
            $course = Course::all()->where('id', '=', 3)->value('name');
            return view('workdays.create', compact('day', 'course_id', 'course'));
        }
        $absences = Absence::all();
        return view('students.absence12', compact('students', 'workdays', 'absences'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreAbsenceRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(Absence $absence)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Absence $absence)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function updateForStudent(UpdateAbsenceRequestElectricBogaloo $request, Student $student)
    {         
        try {
            if (count(WorkDay::all()->where('day', '=', $request->day)->where('course_id', '=', $student->course_id)) > 0) {
                return back()->with('fail', 'This day doesnt have a lesson');            
            }
            $workday_id = DB::table('work_days')->select('id')->where('day', '=', $request->day)->where('course_id', '=', $request->course_id)->get()->value('id');
            DB::update('UPDATE absences SET status = "'.$request->status.'" WHERE student_id = '.$request->student_id.' AND workday_id = '.$workday_id);      
            
        } catch (\Throwable $th) {
            return back()->with('fail', 'This day doesnt have a lesson');            
        }
        return back()->with('success', 'Successfully updated');
    }
    public function update(UpdateAbsenceRequest $request, Student $student)
    {
        DB::update('UPDATE absences SET status = "'.$request->student.'" WHERE student_id = '.$request->userid.' AND workday_id = '.$request->formSub);      
        return back()->with('success', 'Successfully updated');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Absence $absence)
    {
        //
    }
   
}
