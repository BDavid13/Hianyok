<?php

namespace App\Http\Controllers;

use App\Http\Requests\DestroyWorkDayRequest;
use App\Http\Requests\ShowIndexRequest;
use App\Models\Absence;
use App\Models\Course;
use App\Models\Student;
use App\Models\WorkDay;
use App\Http\Requests\StoreWorkDayRequest;
use App\Http\Requests\UpdateWorkDayRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class WorkDayController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $workdays = WorkDay::all();
        return view('workdays.index', ['workdays' => $workdays]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(ShowIndexRequest $request)
    {
        $day = $request->day;
        $course_id = $request->course_id;
        $course = Course::all()->where('id', '=', $request->course_id)->value('name');
        if (count(DB::table('work_days')->where('course_id', '=', $course_id)->where('day', '=', $day)->get()) == 0) {
            return view('workdays.create', compact('day', 'course', 'course_id'));
        } else {
            return back()->with('fail', 'There\'s already a lesson for this day');
        }
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreWorkDayRequest $request, WorkDay $workday)
    {
        if (count(WorkDay::all()->where('day', '=', $request->day)->where('course_id', '=', $request->course_id)) > 0) {
            return redirect('/workdays')->with("fail", "Workday already created for this course");
        }
        $workday = new WorkDay([
            'lesson_name' => $request->lesson,
            'day' => $request->day,
            'course_id' => $request->course_id,
        ]);
        $workday->save();
        $filteredStudents = Student::all()->where('course_id', '=', $request->course_id);

        $filteredStudents->each(function ($student) use ($workday) {
            Absence::create([
                'workday_id' => $workday->id,
                'student_id' => $student->id,
            ]);
        });
        return redirect('/workdays')->with("success", "successfully created workday");
    }

    /**
     * Display the specified resource.
     */
    public function show(WorkDay $workDay)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(WorkDay $workDay)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateWorkDayRequest $request, WorkDay $workDay)
    {
        DB::update('UPDATE work_days SET lesson_name = "' . $request->lesson_name . '" WHERE id = ' . $request->workday_id . ' AND course_id = ' . $request->course_id);
        return back()->with('success', 'Lesson name updated successfully!');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy()
    {
        //
    }
    public function destroy2(DestroyWorkDayRequest $request)
    {
        // try {
            DB::table('work_days')->where('day', '=', $request->day)->where('course_id', '=', $request->course_id)->delete();
        // } catch (\Throwable $th) {
        //     return back()->with('fail', 'something went wrong');
        // }
        return back()->with('success', 'successfully deleted workday');
    }
}
