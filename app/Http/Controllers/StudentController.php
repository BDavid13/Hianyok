<?php

namespace App\Http\Controllers;

use App\Models\Absence;
use App\Models\Student;
use App\Http\Requests\StoreStudentRequest;
use App\Http\Requests\UpdateStudentRequest;
use App\Models\WorkDay;
use Illuminate\Support\Facades\Auth;

class StudentController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $students = Student::all();
        return view('students.index', ['students' => $students]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        if(Auth::user()->role_id == 1){
            return view('students.create');
        }
        return abort(403);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreStudentRequest $request, Student $student)
    {
        if(Auth::user()->role_id == 1){
            $student = new Student(
                [
                    "name" => $request->name,
                    "birthdate" => $request->birthdate,
                    "course_id" => $request->course_id,
                    "created_by" => Auth::user()->id,
                ]
            );
    
            $student->save();
    
            return back()->with("success", "Student created successfully.");
    
        }
        return abort(403);
    }

    /**
     * Display the specified resource.
     */
    public function show(Student $student)
    {
        $absences = Absence::all()->where('student_id', '=', $student->id);
        $workdays = WorkDay::all()->where('course_id', '=', $student->course_id);
        return view('students.show', ['student' => $student], compact('absences', 'workdays'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Student $student)
    {
        return view('students.edit', ['student' => $student]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateStudentRequest $request, Student $student)
    {
        $student->update([
            "name" => $request->name,
            "birthdate" => $request->birthdate,
            "course_id" => $request->course_id,
            "updated_by" => Auth::user()->id,
        ]);
        return back()->with('success', 'Student updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Student $student)
    {
        if(Auth::user()->role_id == 1){
            $student->deleted_by = Auth::user()->id;
            $student->delete();
            return back()->with('success', 'Student deleted successfully');
        }
        return abort(403);
    }

    public function show_deleted(){
        if(Auth::user()->role_id == 1){
            $students = Student::onlyTrashed()->get();
            return view('students.show_deleted', ['students' => $students]);
        }
        return abort(403);
    }

    public function restore(Student $student){
        $student->restore();
        $student->updated_by = Auth::user();
        return back()->with('success', 'Student restored successfully');
    }
}
