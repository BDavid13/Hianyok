<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;

class Absence extends Model
{
    use HasFactory;

    protected $fillable = [
        'status',
        'workday_id',
        'student_id',
    ];

    public function student(): BelongsToMany {
        return $this->belongsToMany(Student::class);
    }
    public function workday(): BelongsToMany {
        return $this->belongsToMany(Workday::class);
    }
}
