<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;

class Student extends Model
{
    use HasFactory;
    use SoftDeletes;


    protected $fillable = [
        'name',
        'birthdate',
        'course_id',
        'created_by',
    ];

    public function course(): BelongsTo{
        return $this->belongsTo(Course::class);
    }

    public function absence(): HasMany{
        return $this->hasMany(Absence::class);
    }
}
